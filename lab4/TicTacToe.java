import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		char[][] board = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };

		printBoard(board);

		int move_count = 0;
		int current_player = 1;

		while(move_count < 9){
			System.out.print("Player" + current_player + " enter row number:");
			int row = reader.nextInt();
			System.out.print("Player" + current_player + " enter column number:");
			int col = reader.nextInt();
			if (row>0 && row<4 && col>0 && col<4 && board[row-1][col-1] == ' '){
				if (current_player == 1) {
					board[row - 1][col - 1] = 'X';
					current_player = 2;
				}
				else{
					board[row - 1][col - 1] = 'O';
					current_player = 1;
				}
				boolean win = checkboard(board,row-1,col-1);
				if (win) {
					if (current_player ==1){
						System.out.println("player" + (current_player+1) + " has won the game!");
						printBoard(board);
						break;
					}
					else{
						System.out.println("player" + (current_player-1) + " has won the game!");
						printBoard(board);
						break;
					}
				}

				printBoard(board);
				move_count++;
			}
			else{
				System.out.println("It's not a valid move!");
			}
		}

		reader.close();
	}

	private static boolean checkboard(char[][] board, int row, int col) {
		char symbol = board[row][col];
		boolean  win = true;
		for(int i = 0;i<3;i++){
			if(board[row][i] != symbol){
				win = false;
			}
			else{
				win = true;
			}
		}
		for(int a = 0;a<3;a++) {
			if (symbol != board[a][col]) {
				win = false;
			}
			else{
				win = true;
			}
		}
		if (row == col){
			win = true;
			for (int i = 0 , a = 0 ; i<3; i++,a++){
				if (symbol != board[i][a]){
					win = false;
				}
			}
		}
		if (row+col == 2) {
			win = true;
			for (int i = 2, a = 0; a < 3; i--, a++) {
				if (symbol != board[i][a]) {
					win = false;
				}
			}
		}
		return win;
	}

	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");

			}
			System.out.println();
			System.out.println("   -----------");

		}

	}

}