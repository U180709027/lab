package stack;

public class StackDemo {
    public static void main(String[] args) {

        Stack stack = new StackArrayImpl();
        stack.push("A");
        stack.push(3);
        stack.push(6);
        stack.push("B");
        stack.push("Y");
        while (!stack.empty()) {
            System.out.println(stack.pop());

        }
    }
}
