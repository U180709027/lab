public class FindMaximum {

	public static void main(String[] args){
		int value1 = Integer.parseInt(args[0]);
        int value2 = Integer.parseInt(args[1]);
        int result;

        boolean someCondition = value1 > value2;

        result = someCondition ? value1 : value2;
		
		if (someCondition) {
			System.out.println("if block executed");
		}else {
			System.out.println("else block executed");
		}
		
	

        System.out.println(result);

	}
}