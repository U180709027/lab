public class FindGrade {

	public static void main(String[] args){
		int score = Integer.parseInt(args[0]);
		String grade; 

		if (score <= 100 && score >= 90)
			grade = "A";
		else if (score >= 80)
			grade = "B";
		else if (score >= 70)
			grade = "C";
		else if (score >= 60)
			grade = "D";
		else 
			grade = "F";
		
		if (score > 100 || score < 0) {
		    grade = "It is not a valid score!";
		}
		
		
		System.out.println(grade);
	}
}